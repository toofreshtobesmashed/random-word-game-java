package sk.murin.randomwordgame;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.Collections;


public class RandomWordGame {
    private JButton btnSubmit;
    private JTextField inputField;
    private int position = 0;
    private String[] words = {"kolobezka", "zralok", "velvyslanectvo", "kvetinarstvo", "telefon", "dvere"};
    private JLabel labelShuffledCharacters;
    private long startTime;
    private String wordFromInputField;

    public RandomWordGame() {

        JFrame frame = new JFrame("Gta 5 moze ist handry fajcit");
        frame.setSize(450, 270);
        frame.setLayout(new BorderLayout());
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);


        JPanel panelTop = new JPanel();
        panelTop.setLayout(new GridLayout(1, 1));
        JPanel panelCenter = new JPanel();
        panelCenter.setLayout(new GridLayout(1, 1));
        JPanel panelBottom = new JPanel();
        panelBottom.setLayout(new FlowLayout());

        JLabel labelMainMsg = new JLabel("Uhadni slovo");
        labelMainMsg.setFont(new Font("Times new Roman", Font.BOLD, 28));
        labelMainMsg.setHorizontalAlignment(SwingConstants.CENTER);
        panelTop.add(labelMainMsg);

        JMenuBar menuBar = new JMenuBar();
        JMenuItem itemReset = new JMenuItem("Reset");
        JMenu menu = new JMenu("Moznosti");
        itemReset.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                reset();
            }
        });

        //odtialtoooooooooooooooooooooooooooooooooooooooo
        menuBar.add(menu);
        menu.add(itemReset);
        frame.setJMenuBar(menuBar);

        labelShuffledCharacters = new JLabel("");
        labelShuffledCharacters.setFont(new Font("Times new Roman", Font.BOLD, 30));
        labelShuffledCharacters.setVerticalAlignment(SwingConstants.CENTER);
        labelShuffledCharacters.setHorizontalAlignment(SwingConstants.CENTER);

        panelCenter.add(labelShuffledCharacters);

        inputField = new JTextField();
        inputField.setColumns(8);
        inputField.setFont(new Font("Times new Roman", Font.BOLD, 28));

        btnSubmit = new JButton("Potvrd");
        btnSubmit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                check();
                shuffleCharacters();
            }
        });

        panelBottom.add(inputField);
        panelBottom.add(btnSubmit);

        frame.add(panelTop, BorderLayout.NORTH);
        frame.add(panelCenter, BorderLayout.CENTER);
        frame.add(panelBottom, BorderLayout.SOUTH);

        frame.setResizable(false);
        frame.setVisible(true);
        shuffleCharacters();
        startTime = System.currentTimeMillis();
    }


    public void shuffleCharacters() {
        try {
            wordFromInputField = words[position];
        } catch (Exception e) {
        }

        Character[] characters = new Character[wordFromInputField.length()];

        for (int i = 0; i < wordFromInputField.length(); i++) {
            characters[i] = wordFromInputField.charAt(i);
        }

        Collections.shuffle(Arrays.asList(characters));
        String newWord = "";
        for (int i = 0; i < characters.length; i++) {
            newWord += characters[i] + " ";
        }
        labelShuffledCharacters.setText(newWord);

    }

    private void check() {
        String text = inputField.getText();
        if (text.equals(wordFromInputField)) {
            JOptionPane.showMessageDialog(null, "Uhádol si", "Huraaaa...", JOptionPane.INFORMATION_MESSAGE);
            labelShuffledCharacters.setText(words[position]);
            position++;
            if (position == words.length) {
                long total = (System.currentTimeMillis() - startTime) / 1000;
                System.out.println(total);
                JOptionPane.showMessageDialog(null, "Koniec, trvalo ti to " + total + " sekund", "Koniec!...", JOptionPane.INFORMATION_MESSAGE);
                btnSubmit.setEnabled(false);
            }
            inputField.setText("");
        }
    }


    public void reset() {
        position = 0;
        startTime = System.currentTimeMillis();
        Collections.shuffle(Arrays.asList(words));
        labelShuffledCharacters.setText(words[position]);
        shuffleCharacters();
        btnSubmit.setEnabled(true);
    }
}

